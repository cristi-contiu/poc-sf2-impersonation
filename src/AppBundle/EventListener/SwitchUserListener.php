<?php
// src/AppBundle/EventListener/SwitchUserListener.pnp
namespace AppBundle\EventListener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;

class SwitchUserListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * Triggered during user impersonation request
     *
     * @param   SwitchUserEvent  $event
     * @throws  AccessDeniedException
     */
    public function onSwitchUser(SwitchUserEvent $event)
    {
        // Always allow user to switch back
        // Alternative: we could get and compare the original user to the target
        if ($event->getRequest()->query->get('impersonate') === '_exit') {
            return;
        }

        $child = $event->getTargetUser();
        $parent = $this->tokenStorage->getToken()->getUser();
        
        if (false == $this->checkUserParent($child, $parent)) {
            throw new AccessDeniedException();
        }
    }

    /**
     * Service injector
     *
     * @param  TokenStorageInterface  $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage = null)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Checks if the provided parent actually is a parent of the child user
     *
     * @param   UserInterface  $child
     * @param   UserInterface  $parent
     * @return  boolean
     */
    protected function checkUserParent(UserInterface $child, UserInterface $parent)
    {
        // logic for checking the child's parents goes here
        $childName = $child->getUsername();
        $parentName = $parent->getUsername();
        $parents = array(
            'mona'   => array('parent'),
            'lisa'   => array('parent'),
            'vlad'   => array('admin'),
            'parent' => array(),
            'admin'  => array(),
        );

        return in_array($parentName, $parents[$childName]);
    }
}